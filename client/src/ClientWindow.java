import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener {

    private static final String IP_ADDR = "192.168.0.12";
    private static final int PORT = 8189;
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ClientWindow::new);
    }

    private final JTextArea log = new JTextArea();
    private final JTextField fieldNickname = new JTextField("Гость");
    private final JTextField fieldInput = new JTextField();

    private TCPConnection connection;

    private ClientWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
        setAlwaysOnTop(true);

        // Устанавливает заголовок окну
        setTitle("Ламповый чатик");

        /*
            Задаем иконку для окна

            getResource(String) по имени ресурса возвращает java.net.URL,
            через который можно получить этот ресурс.
            Данные для каждого объекта содержат ссылку на объект класса java.lang.Class,
            и это возвращается методом getClass.

            Я пыталась, честно, но, короче, без getClass тут не работает :(
         */
        URL iconURL = getClass().getResource("img/lamp.png");
        ImageIcon icon = new ImageIcon(iconURL);
        setIconImage(icon.getImage());

        add(fieldNickname, BorderLayout.NORTH);

        log.setEditable(false);
        log.setLineWrap(true);
        add(log, BorderLayout.CENTER);

        //Создаем и добавляем скролл-панель
        JScrollPane jScrollPanel = new JScrollPane(log);
        add(jScrollPanel);

        //Создаем новую панель
        JPanel bottomPanel = new JPanel(new BorderLayout());
        add(bottomPanel, BorderLayout.SOUTH);

        //Создаем кнопку "Отправить" и добавляем на панель
        JButton jbSendMessage = new JButton("Отправить");
        bottomPanel.add(jbSendMessage, BorderLayout.EAST);

        //Добавляем текстовое поле на панель
        bottomPanel.add(fieldInput, BorderLayout.CENTER);

        //Добавляем дейсвия к кнопке и текстовому полю
        jbSendMessage.addActionListener(this);
        fieldInput.addActionListener(this);

        jbSendMessage.setBackground(Color.LIGHT_GRAY);
        fieldNickname.setBackground(Color.LIGHT_GRAY);

        setVisible(true);
        try {
            connection = new TCPConnection(this, IP_ADDR, PORT);
        } catch (IOException e) {
            printMessage("Connection exception: " + e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String msg = fieldInput.getText();
        if (msg.equals("")) return;
        fieldInput.setText(null);
        connection.sendString(fieldNickname.getText() + ": " + msg);
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMessage("Connection ready...");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMessage(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMessage("Connection close");
    }

    @Override
    public void onException(TCPConnection tcpConnection, IOException e) {
        printMessage("Connection exception: " + e);
    }

    private synchronized void printMessage(String message) {
        SwingUtilities.invokeLater(() -> {
            log.append(message + "\n");
            log.setCaretPosition(log.getDocument().getLength());
        });
    }
}
